<?php
//sertakan model
include_once("model/ModelSiswa.php");
class Controller{
  public $model;
  public function __construct(){
    $this->model = new ModelSiswa();
  }
  public function index(){
    $siswas = $this->model->getAll();
    include 'view/list_siswa.php';
  }
  public function detail(){
    //jika ada parameter, tampilkan siswa dengan id tertentu
    $siswa = $this->model->getById($_GET['nis']);
    include 'view/detail_siswa.php';
  }
  public function add(){
    if(isset($_POST['nis'])){
      //simpan data,setelah sukses redirect
      $data = $this->bacaParameter();
      $this->model->simpan($data);
      header("location:index.php");
    }
    //tampilkan form
    $action="index.php?a=add";
    include 'view/form_siswa.php';
  }

  public function update(){
    $data = $this->model->getById($_GET['nis']);
    $readonly="readonly";
    if(isset($_POST['nis'])){
      //simpan data,setelah sukses redirect
      $data = $this->bacaParameter();
      $this->model->update($data);
      header("location:index.php");
    }
    $action="index.php?a=update";
    //tampilkan form
    include 'view/form_siswa.php';
  }

  public function delete(){
    $this->model->delete($_GET['nis']);
    header("location:index.php");
  }

  private function bacaParameter(){
    // tangkap request
    $_nis = $_POST['nis'];
    $_nama = $_POST['nama'];
    $_tmp_lahir = $_POST['tmp_lahir'];
    $_tgl_lahir = $_POST['tgl_lahir'];
    $_gender = $_POST['gender'];
    $_alamat = $_POST['alamat'];
    $_telpon = $_POST['telpon'];
    $_ayah = $_POST['ayah'];
    $_ibu = $_POST['ibu'];
    $_proses = $_POST['proses'];
    // simpan ke array,perhatikan urutan data
    $data=[$_nama,$_tmp_lahir,$_tgl_lahir,
     $_gender,$_alamat,$_telpon,$_ayah,$_ibu,$_nis];
    return $data;
  }

}

?>
