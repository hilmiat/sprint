<h4><a href="index.php">Home</a></h4>
<form class="form-horizontal"
      action=<?=$action?>
      method="POST">
<fieldset>
<!-- Form Name -->
<legend>Form Entry Data Siswa</legend>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nis">NIS</label>
  <div class="col-md-4">
  <input id="nis" name="nis" placeholder="NIS" class="form-control input-md" required="" type="text"
  value="<?=$data->nis?>" <?=$readonly?>>

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama Lengkap</label>
  <div class="col-md-5">
  <input id="nama" name="nama" placeholder="Nama Lengkap"
  class="form-control input-md" required="" type="text" value="<?=$data->nama?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tmp_lahir">Tempat Lahir</label>
  <div class="col-md-5">
  <input id="tmp_lahir" name="tmp_lahir" placeholder="Tempat Lahir"
  class="form-control input-md" type="text" value="<?=$data->tmp_lahir?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tgl_lahir">Tanggal Lahir</label>
  <div class="col-md-4">
  <input class="form-control" id="date" name="tgl_lahir" placeholder="MM/DD/YYY"
  type="text" value="<?=$data->tgl_lahir?>"/>


</div>
<!--
  <input id="tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" class="form-control input-md" type="text">

  </div>
  -->
</div>


<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="gender">Gender</label>
  <?php
   if($data->gender=='L'){
   	  $cek_laki="checked";
   }else{
   	  $cek_perempuan="checked";
   }
  ?>
  <div class="col-md-4">
    <label class="radio-inline" for="gender-0">
      <input name="gender" id="gender-0"
      value="L" type="radio" <?=$cek_laki?>>
      Laki-Laki
    </label>
    <label class="radio-inline" for="gender-1">
      <input name="gender" id="gender-1" value="P" type="radio" <?=$cek_perempuan?>>
      Perempuan
    </label>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="alamat">Alamat</label>
  <div class="col-md-4">
    <textarea class="form-control" id="alamat" name="alamat"><?=$data->alamat?></textarea>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="telpon">Telpon</label>
  <div class="col-md-4">
  <input id="telpon" name="telpon" placeholder="Telpon"
  class="form-control input-md" type="text" value="<?=$data->telpon?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ayah">Nama Ayah</label>
  <div class="col-md-4">
  <input id="ayah" name="ayah" placeholder="Nama Ayah"
    class="form-control input-md" type="text" value="<?=$data->nama_ayah?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ibu">Nama Ibu</label>
  <div class="col-md-4">
  <input id="ibu" name="ibu" placeholder="Nama Ibu"
    class="form-control input-md" type="text" value="<?=$data->nama_ibu?>">

  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="simpan"></label>
  <div class="col-md-8">
  <?php
   if(!empty($_nis)){
   	 $tombol = 'Update';
   }else {
   	 $tombol = 'Simpan';
   }
  ?>
    <input type="submit" id="simpan" name="proses" class="btn btn-success" value="<?=$tombol?>"/>
    <input type="submit" id="hapus" name="proses" class="btn btn-danger" value="Hapus"/>
  </div>
</div>

</fieldset>
</form>
