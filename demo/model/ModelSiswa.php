<?php
include_once ("model/Model.php");
include_once ("model/Siswa.php");
class ModelSiswa extends Model{
  public function __construct(){
    $this->tblName = "siswa";
    parent::__construct();
  }
  public function getAll(){
    $rs = parent::getAll();
    $data = array();
    foreach ($rs as $row) {
      $siswa = new Siswa();
      $siswa->setAttribute($row);
      $data[$row['nis']] = $siswa;
    }
    return $data;
  }

  public function simpan($data){
    // 1) buat query
    $sql = "INSERT INTO siswa
         (nama,tmp_lahir,tgl_lahir,gender,
         alamat,telpon,nama_ayah,nama_ibu,nis) VALUES
         (?,?,?,?,?,?,?,?,?)";
    parent::execute($sql,$data);
  }
  public function update($data){
    // 1) buat query
    $sql = "UPDATE siswa SET nama=?,tmp_lahir=?,tgl_lahir=?,gender=?,
         alamat=?,telpon=?,nama_ayah=?,nama_ibu=? WHERE nis=?";

    parent::execute($sql,$data);
  }

  public function delete($nis){
    $sql = "DELETE FROM siswa WHERE nis=?";
    parent::execute($sql,array($nis));
  }

}
?>
