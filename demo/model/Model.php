<?php
include_once("config/dbkoneksi.php");
class Model{
  private $db;
  protected $tblName;
  public function __construct(){
    //$this->db = $dbh;
    $this->db = DbKoneksi::getInstance();
  }
  public function getAll(){
    $sql = "SELECT * FROM ".$this->tblName;
    // $sql = "SELECT * FROM siswa";
    $rs = $this->db->query($sql);
    return $rs;
  }

  public function getById($id){
    $data = $this->getAll();
    return $data[$id];
  }

  public function execute($sql,$data){
    // 2) buat preparestatement
 	  $st = $this->db->prepare($sql);
    // 3) eksekusi prepare statement
 	  $st->execute($data);
  }

}
