<?php
class MY_Controller extends CI_Controller{
  function __construct(){
    parent:: __construct();
    //load library/helper/Model
    //load model/user/UserModel.php
    $this->load->model('user/UserModel');
    //$this->load->library('session');
    $this->load->helper('url');
  }
}
