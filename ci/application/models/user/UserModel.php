<?php

class UserModel extends CI_Model{
  function __construct(){
    parent::__construct();
    //load database
    $this->load->database();
  }
  //getAllUser
  function get_all_user(){
    //select * from users;
    return $this->db->get('users');
  }
  //update User
  function update_user($id,$data){
    //seleksi user yg akan diupdate
    $this->db->where('id',$id);
    //update
    if($this->db->update('users',$data)){
      //jika sukses update, return true
      return true;
    }else{
      //jika gagal, return false;
      return false;
    }
  }
  //view User
  function get_user($id){
    $this->db->where('id',$id);
    $result = $this->db->get('users');
    if($result){
      return $result;
    }else{
      return false;
    }
  }
  
  //delete User
  function delete_user($id){
    return $this->db->where('id',$id)
                    ->delete('users');
  }
  //create User
  function create_user($data){
    //insert into users(first_name...) values (...)
    if($this->db->insert('users',$data)){
      return true;
    }else{
      return false;
    }
  }

  //cek login
  function login($data){
    $this->db->where('email',$data['email']);
    $this->db->where('password',$data['password']);
    $query = $this->db->get('users');
    if ($query->num_rows()==1){
      return $query;
    }else{
      return false;
    }
  }

}
?>
