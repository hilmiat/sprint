<?php
class Siswa_model extends CI_Model{
    public function __construct(){
      //load database
      $this->load->database();
    }
    public function getSiswa(){
      //query table siswa
      $query = $this->db->get('siswa');
      return $query->result_array();
    }
    public function getDetailSiswa($nis){
      $query = $this->
          db->get_where('siswa',array('nis'=>$nis));
      return $query->row_array();
    }

}
