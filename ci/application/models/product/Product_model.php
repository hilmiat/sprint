<?php
class Product_model extends CI_Model{
  function __construct(){
    parent:: __construct();
    $this->load->database();
  }

  public function getAll(){
    return $this->db
      ->select('products.*,categories.name as category')
      ->from('products')
      ->join('categories',
          'categories.id=products.category_id')
      ->get();
    // return $this->db->get('products');
  }
  public function getByCat($id_cat){
    return $this->db
      ->select('products.*,categories.name as category')
      ->from('products')
      ->join('categories',
          'categories.id=products.category_id')
      ->where('products.category_id',$id_cat)
      ->get();
    // return $this->db->get('products');
  }
  public function get_detail($id){
    return
      $this->db->where('id',$id)->get('products');
  }
  public function insert($data){
    return $this->db->insert('products',$data);
  }
  public function get_categories(){
    $query = $this->db->get('categories');
    $rows = $query->result();
    foreach($rows as $row){
      $cat[$row->id] = $row->name;
    }
    return $cat;
  }
}
