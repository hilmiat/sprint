<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My OlShop</title>
    <!-- Bootstrap -->
    <link href="<?=base_url('bootstrap/css/bootstrap.min.css')?>"
        rel="stylesheet">

  </head>
  <body>

<?php
	$isAdmin = FALSE;
	$isLogin = FALSE;
	if($this->session->userdata('logged_in')){
		$isLogin = TRUE;
		$user = $this->session->userdata('user');
		if($user->access_level < 2){
			$isAdmin = TRUE;
		}
	}
?>
    <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="#">My Ol Shop</a> -->
						<?=anchor('product/product',
											'My Ol Shop',
											array('class'=>'navbar-brand'))?>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><?=anchor('product/product','Home')?></li>
							<?php if($isLogin && $isAdmin):?>
              <li><?=anchor('user/users','Manage User')?></li>
							<li><?=anchor('product/product/admin','Manage Product')?></li>
							<?php endif ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li>
							<?php
								if($isLogin){
									echo '<li>';
									echo anchor('user/register/logout','LogOut');
									echo '</li>';
									echo '<li>';
						echo anchor('user/profil',$user->first_name);
									echo '</li>';
								}else{
									echo '<li>'
									.anchor('user/register/login','Login').
									'</li>';
								}
							?>
							<li>
								<?=anchor('product/cart',
										'Cart <span class="badge">'
										.$this->cart->total_items().
										'</span>'
										)?>
							</li>
							</li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
			<div class="container">
