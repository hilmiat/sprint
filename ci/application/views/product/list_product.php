<h1>Daftar Product</h1>
<h4>Category:
<?php
  $cat =$this->Product_model->get_categories();
  $cat['0'] = 'All';
  echo form_dropdown(
            'category',
            $cat,
            $pil,
            array('onchange'=>'
            pindah(this.options[this.selectedIndex].value)')
          );
?>
</h4>
<script>
  function pindah(loc){
    console.log("pergi ke"+loc);
    window.document.location
      .href="<?=site_url('prod')?>/"+loc;
  }
</script>
<div id="body">
  <div class="row">
    <?php foreach($query->result() as $row):?>
      <div class="col-md-4">
        <div class="well">
          <h2><?=$row->name?></h2>
          <span class="glyphicon glyphicon-usd"></span>
          <?=$row->price?><br>
          <span class="glyphicon glyphicon-tags"></span>
          <?=$row->category?><br>

          <?=anchor('product/cart/add/'.$row->id,
                    'Buy item',
                     array('class'=>'btn btn-primary')
                    )?>
          <?=anchor('product/product/detail/'.$row->id,
                    'View Detail',
                    array('class'=>'btn btn-primary')
                    )?>

        </div>
      </div>
    <?php endforeach ?>
  </div>
</div>
