<h1>Manage Products</h1>
<div id="body">
<?=anchor('product/product/add','add')?>

<table>
  <thead>
    <tr>
      <th>#</th>
      <th>Item Name</th>
      <th>Code</th>
      <th>Price</th>
      <th>Category</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php if($query->num_rows() > 0):?>
      <?php foreach ($query->result() as $row) : ?>
        <tr>
          <td><?=$row->id?></td>
          <td><?=$row->name?></td>
          <td><?=$row->code?></td>
          <td><?=$row->price?></td>
          <td><?=$row->category?></td>
          <td>
              <?=anchor('product/product/delete/'.$row->id,
                        'delete',
                        array('onclick'=>
                            "return confirm('Mau hapus?')")
                        )?>
              |

              <?=anchor('product/product/update/'.$row->id,
               'update')?>
            </td>
        </tr>
      <?php endforeach; ?>
    <?php else : ?>
      <tr>
        <td colspan="5">Belum ada product!</td>
      </tr>
    <?php endif;?>
  </tbody>
</table>
</div>
