<h1>Shopping Cart</h1>
<div id="body">
  <table class="table table-striped ">
    <thead>
      <tr>
        <th>No.</th>
        <th>Item</th>
        <th>Price</th>
        <th>Qty</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      foreach($this->cart->contents() as $item) :
      ?>
        <tr>
          <td><?=$no?></td>
          <td><?=$item['name']?></td>
          <td align="right">
            <?=number_format($item['price'],2,',','.')?>
          </td>
          <td><?=$item['qty']?></td>
          <td align="right">
            <?=number_format($item['price']*$item['qty'],
                              2,',','.')?></td>
        </tr>
      <?php
      $no++;
      endforeach
      ?>
      <tfoot>
        <tr>
          <td colspan="4">Total</td>
          <td align="right">
          <?=number_format($this->cart->total()
                    ,2,',','.')?>
          </td>
        </tr>
      </tfoot>
    </tbody>
  </table>
  <?=anchor('product/cart/clear','Clear Cart')?>
  |
  <?=anchor('product/cart/co','Check Out')?>
  |
  <?=anchor('product/product','Continue Shopping')?>

</div>
