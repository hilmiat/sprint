<h1>Add Product</h1>
<div id="body">
<?php
//tampilkan pesan error
echo validation_errors();

echo form_open('product/product/add');

echo '<h3><b>Item Name:</b></h3>';
echo form_input('name',set_value('name'));

echo '<h3><b>Code:</b></h3>';
echo form_input('code',set_value('code'));

echo '<h3><b>Price:</b></h3>';
echo form_input('price',set_value('price'));

echo '<h3><b>Category:</b></h3>';
// $options = array(
//     '1' => 'Electronics',
//     '2' => 'Furnitures'
// );
$options = $this->Product_model->get_categories();
echo form_dropdown('category_id',
                  $options,
                  set_value('category_id'));

echo '<h3><b>Description:</b></h3>';
echo form_textarea('description',set_value('description'));
echo '<br><br>';
echo form_submit('submit',"Add Product");
?>
</div>
