<div class="row">
  <div class="col-md-4">
<h2 class="form-signin-heading">Login User</h2>



<?=validation_errors(
      '<div class="alert alert-danger" role="alert">',
      '</div>')?>


<?php
if(isset($status)){
  echo $status;
}
echo form_open('user/register/login',
  array('class'=>'form-signin'));
// echo '<h3><b>Email:</b></h3>';
echo form_input('email','',
  array('class'=>'form-control',
        'placeholder'=>'Email')
      );
// echo '<h3><b>Password:</b></h3>';
echo form_password('password','',
      array('class'=>'form-control',
      'placeholder'=>'Password')
    );
// echo '<br><br>';
echo '<label>Doesnt have account?'
  .anchor('user/register','create here').'</label>';
echo form_submit('submit',"Login",
    array('class'=>'btn btn-lg btn-primary btn-block'));

echo anchor('user/register/loginfb',"Login with Facebook",
        array('class'=>'btn btn-lg btn-primary btn-block'));


?>

</div>
</div>
