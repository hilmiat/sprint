<h1>Register User</h1>
<div id="body">
<?=validation_errors(
        '<div class="alert alert-danger" role="alert">',
        '</div>')
?>
<?php
//tampilkan pesan error


echo form_open('user/register',
      array('class'=>'form-signin')
);
echo '<h3><b>First Name:</b></h3>';
echo form_input2('first_name',set_value('first_name'));
echo '<h3><b>Last Name:</b></h3>';
echo form_input2('last_name',set_value('last_name'));
echo '<h3><b>Email:</b></h3>';
echo form_input2('email',set_value('email'));

echo '<h3><b>Password:</b></h3>';
echo form_password2('password',set_value('password'));
echo '<h3><b>Repeat Password:</b></h3>';
echo form_password2('pass_repeat',set_value('pass_repeat'));
echo '<br><br>';
echo form_submit('submit',"Register");
?>
</div>
