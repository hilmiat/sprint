<?php
//tampilkan pesan error
echo validation_errors();
$action = ($proses=='update')?'user/users/proses_update':
        'user/users/add';
echo form_open($action);

echo '<h3><b>First Name:</b></h3>';
echo form_input('first_name',
                set_value('first_name',$first_name));

echo '<h3><b>Last Name:</b></h3>';
echo form_input('last_name',
                set_value('last_name',$last_name));

echo '<h3><b>Email:</b></h3>';
echo form_input('email',
                set_value('email',$email));

if($proses!='update'){
  echo '<h3><b>Password:</b></h3>';
  echo form_password('password',set_value('password'));
}

echo '<h3><b>Access Level:</b></h3>';
echo form_input('access_level',
      set_value('access_level',$access_level));

echo '<h3><b>Address:</b></h3>';
echo form_textarea('address',
      set_value('address',$address));

echo form_hidden('id',$id);

$tbl = ($proses=='update')?'Update user':'Add User';
echo form_submit('submit',$tbl);
