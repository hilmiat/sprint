<h1><?=$judul?></h1>
<div id="body">
<?=anchor('user/users/add','add')?>

<?=$status?>
<table>
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php if($query->num_rows() > 0):?>
      <?php foreach ($query->result() as $row) : ?>
        <tr>
          <td><?=$row->id?></td>
          <td><?=$row->first_name?></td>
          <td><?=$row->last_name?></td>
          <td><?=$row->email?></td>
          <td>
              <?=anchor('user/users/delete/'.$row->id,
                        'delete',
                        array('onclick'=>
                            "return confirm('Mau hapus?')")
                        )?>
              |

              <?=anchor('user/users/update/'.$row->id,
               'update')?>
            </td>
        </tr>
      <?php endforeach; ?>
    <?php else : ?>
      <tr>
        <td colspan="5">Belum ada user!</td>
      </tr>
    <?php endif;?>
  </tbody>
</table>
</div>
