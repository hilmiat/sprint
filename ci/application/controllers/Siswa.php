<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('siswa_model');
    $this->load->helper('url');
    $this->load->helper('html');
  }

  public function index(){
      $title = "Daftar Nama Siswa";
      $siswas = $this->siswa_model->getSiswa();
      $this->load->view('template/header');
      $this->load->view('siswa/list_siswa',
          array('siswas'=>$siswas,
                'judul'=>$title));
      $this->load->view('template/footer');
  }
  public function detail($id){
    $data['judul'] = "Detail Siswa ke-".$id;
    $data['siswa'] = $this->siswa_model->
          getDetailSiswa($id);
    $this->load->view('template/header');
    $this->load->view('siswa/detail_siswa',$data);
    $this->load->view('template/footer');
  }
  public function add(){
    $this->load->view('welcome/depan');
  }
}
