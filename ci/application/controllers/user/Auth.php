<?php
class Auth extends My_Controller{
  private $fb;
  function __construct(){
    parent::__construct();
    //load class
        require_once APPPATH.
            "libraries/Facebook/Facebook.php";
        require_once APPPATH.
            "libraries/Facebook/Exceptions/FacebookSDKException.php";
        require_once APPPATH.
                "libraries/Facebook/autoload.php";
        $this->fb = new Facebook\Facebook(
            array(
                'app_id'=>'1312704758773711',
                'app_secret'=>'40b98b1772cf23ef28f0677be3953e0d'
                  )
        );
  }
  function loginfb(){
      $login_url = $this->fb->getRedirectLoginHelper()
              ->getLoginUrl(site_url('user/auth/callback'),
                  ['email']);
      redirect($login_url);
  }
  function callback(){
      $helper = $this->fb->getRedirectLoginHelper();
      try{
        $token = $helper->getAccessToken();
        //get profil user
        $profil = $this->fb->get('/me',$token)
            ->getGraphUser();
//
        $user['first_name'] = $profil['name'];
        // $user['last_name'] = $profil['last_name'];
        // $user['email'] = $profil['email'];
        $user['access_level'] = 2;

        // die(print_r($profil['name']));

        $this->session->set_userdata('logged_in',TRUE);
        $this->session->set_userdata('user',$user);
        $this->session->set_userdata('token',$token);
        redirect('user/profil');
      }catch(Facebook\Exceptions\FacebookResponseException $e){
        $data['exception']="Response error";
        $data['message']=$e->getMessage();
        $this->load
          ->view('errors/html/error_exception',$data);
      }catch(Facebook\Exceptions\FacebookSDKException $e){
        $data['exception']="Response error";
        $data['message']=$e->getMessage();
        $this->load
          ->view('errors/html/error_ecxeption',$data);
      }

  }



}
