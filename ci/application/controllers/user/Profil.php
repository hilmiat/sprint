<?php
class Profil extends MY_Controller{
  function __construct(){
    parent:: __construct();
    //batasi,agar tidak diakses oleh user yang blm login
    if( ($this->session->userdata('logged_in')==FALSE)
|| ($this->session->userdata('user')->access_level > 2))
      {
        redirect('user/register/login');
      }
  }
  public function index(){
    $data['judul'] = "Profil User";
    //baca data tentang user dari session
    $data['user'] = $this->session->userdata('user');
    //$user = $this->session->user
    $this->load->view('template/header');
    $this->load->view('user/profil',$data);
    $this->load->view('template/footer');
  }
}
