<?php
class Users extends MY_Controller{
  function __construct(){
    parent:: __construct();
    $this->load->library('form_validation');
    $this->load->helper('security');
    if( ($this->session->userdata('logged_in')==FALSE)
|| ($this->session->userdata('user')->access_level > 1))
      {
        redirect('user/profil');
      }
  }

  //index --> list all user
  public function index(){
      $data['judul'] = "Manage User";
      $data['query'] = $this->UserModel
          ->get_all_user();
      $data['status'] = $this->session
                            ->flashdata('STATUS');
      //tampilkan pada view
      $this->load->view('template/header');
      $this->load->view('user/manage_user',$data);
      $this->load->view('template/footer');
  }

  //delete -->delete user by id
  public function delete($id){
    if($this->UserModel->delete_user($id)==FALSE){
        $this->session
          ->set_flashdata('STATUS', 'Gagal hapus Data');
    }else{
        $this->session
          ->set_flashdata('STATUS', 'Sukses hapus Data');
    }
    redirect('user/users');
  }

  public function add(){
    $this->form_validation
            ->set_rules($this->createRules());
    if($this->form_validation->run()){
      $data = $this->bacaData();
      $data['password'] = do_hash($data['password']);
      if($this->UserModel->create_user($data)){
        redirect('user/users');
      }
    }
    //tampilkan form
    $this->load->view('template/header');
    $this->load->view('user/create');
    $this->load->view('template/footer');
  }

  public function update($id){
    //cari di database user dgn id tertentu
    $result = $this->UserModel->get_user($id);
    if($result){
      $data = $result->row_array();
      $data['proses'] = 'update';
      //tampilkan data pada form
      $this->load->view('template/header');
      $this->load->view('user/create',$data);
      $this->load->view('template/footer');
    }else{

    }
  }

  public function proses_update(){
    $rules = $this->createRules();
    unset($rules[1]);
    $this->form_validation
            ->set_rules($rules);
    if($this->form_validation->run()){
        //baca id dari hidden field
        $id = $this->input->post('id');
        $data = $this->bacaData();
        if($this->UserModel->update_user($id,$data)){
          redirect('user/users');
        }
    }else{

    }
  }

  private function createRules(){
    $rules = array(
        array('field'=>'first_name',
          'label'=>'First Name',
          'rules'=>'required|max_length[125]'),
        array('field'=>'email',
          'label'=>'Email',
          'rules'=>'valid_email|is_unique[users.email]'),
        array('field'=>'last_name',
          'label'=>'Last Name',
          'rules'=>'required|max_length[125]')
    );
    return $rules;
  }

  private function bacaData(){
    $fields = array('first_name',
                    'last_name',
                    'email',
                    'password',
                    'address',
                    'access_level');
    foreach($fields as $field){
      $data[$field] = $this->input->post($field);
    }
    return $data;
  }
}
