<?php
class Register extends MY_Controller{
  function __construct(){
    parent:: __construct();
    //$this->load->library('encryption');
    $this->load->helper('security');
    $this->load->library('form_validation');
    $this->load->helper('form');
  }

  function index(){
    //baca data dari form
    $pass = $this->input->post('password');
    $encr = do_hash($pass);
    $data = array(
      //'first_name'=>$_POST['first_name'],
      'first_name' => $this->input->post('first_name'),
      'last_name' => $this->input->post('last_name'),
      'email' => $this->input->post('email'),
      'password' => $encr,
      'access_level' => 2
    );

    $this->form_validation
        ->set_rules('first_name','First Name',
            'required|max_length[125]',
             array('required'=>'You must provide a %s!!',
                   'max_length'=>'%s max 125'
                  )
          );
    $this->form_validation
        ->set_rules('password','Password','required',
                array('required'=>'You must provide a %s!!')
              );
    $this->form_validation
        ->set_rules('pass_repeat','Password Confirmation',
            'required|matches[password]',
            array('required'=>'You must provide a %s!!',
                  'matches'=>'%s not match')
          );

    if($this->form_validation->run()){
      //simpan
      if($this->UserModel->create_user($data)){
        redirect('/user/Users/index');
      }
    }
    //tampilkan form
    $this->load->view('template/header');
    $this->load->view('user/register');
    $this->load->view('template/footer');
  }

  function login(){
    $this->form_validation->set_rules('email',
                                      'Email',
                                      'required|trim');
    $this->form_validation->set_rules('password',
                                      'Password',
                                      'required|trim');
    $status='';
    if($this->form_validation->run()){
      $data['email'] = $this->input->post('email');
      $pass = $this->input->post('password');
      $data['password'] = do_hash($pass);
      //cek di database
      $query = $this->UserModel->login($data);
      if($query){
        //baca data user, lalu simpan di Session
        $user = $query->row();
        $this->session->set_userdata('user',$user);
        $this->session->set_userdata('logged_in',TRUE);
        //redirect
        if($user->access_level==1){
          //admin
          redirect('user/users');
        }else{
          //user biasa
          redirect('user/profil');
        }

      }else{
        $status = 'Login Failed';
      }
    }

    //tampilkan form login
    $this->load->view('template/header');
    $this->load->view('user/login',array('status'=>$status));
    $this->load->view('template/footer');
  }

  function logout(){
    $this->session->unset_userdata('user');
    $this->session->set_userdata('logged_in',FALSE);
    redirect('user/register/login');
  }




}
