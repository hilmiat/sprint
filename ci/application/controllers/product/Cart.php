<?php
class Cart extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->library('cart');
    $this->load->model('product/Product_model');
    $this->load->library('cart');
    $this->load->helper('url');
  }

  public function add($id){
    $query = $this->Product_model->get_detail($id);
    $prod = $query->row();
    $data = array(
      'id' => $prod->id,
      'qty' => 1,
      'price' => $prod->price,
      'name'  => $prod->name
    );
    $this->cart->insert($data);
    redirect('product/cart');
  }
  public function index(){
    $this->load->view('template/header');
    $this->load->view('product/display_cart');
    $this->load->view('template/footer');
  }
  //co
  public function co(){
    //simpan data ke tabel orders
    
  }
  //clear
  public function clear(){
    $this->cart->destroy();
    redirect('product/product');
  }
}
