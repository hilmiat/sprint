<?php
class Product extends CI_controller{
  function __construct(){
    parent:: __construct();
    $this->load->model('product/Product_model');
    $this->load->helper('url');
    $this->load->library('form_validation');
    //ketahui action apa yg direquest
    $action = $this->router->method;
    $khusus_admin = array('add','admin','update','delete');
    $isNotLogin = ($this->session->userdata('logged_in')==FALSE);
    $isNotAdmin=TRUE;
    if(!$isNotLogin){
      $isNotAdmin = ($this->session->userdata('user')->access_level > 1);
    }
    if(
      in_array($action,$khusus_admin) &&
      ($isNotLogin||$isNotAdmin)){
          redirect('user/profil');
    }

  }
  public function index($id_cat=0){
    if($id_cat==0){
      $query = $this->Product_model->getAll();
    }else{
      $query = $this->Product_model
          ->getByCat($id_cat);
    }
    $data['query'] =  $query;
    $data['pil'] = $id_cat;
    $this->load->view('template/header');
    $this->load->view('product/list_product',$data);
    $this->load->view('template/footer');
  }
  public function add(){
    //set validasi data
    $this->form_validation
        ->set_rules($this->get_rules());
    if($this->form_validation->run()){
      //jika valid, baca data
      $data = $this->baca_data();
      //simpan ke db
      if($this->Product_model->insert($data)){
        //jika sukses redirect
        redirect('product/product/admin');
      }
    }
    //tampilkan form
    $this->load->view('template/header');
    $this->load->view('product/form_product');
    $this->load->view('template/footer');
  }
  private function get_rules(){
    return
      array(
        array('field'=>'name',
              'label'=>'name',
              'rules'=>'required'),
        array('field'=>'code',
              'label'=>'code',
              'rules'=>'required|numeric'),
        array('field'=>'price',
              'label'=>'Price',
              'rules'=>'required|numeric')
      );
  }
  private function baca_data(){
    $fields = array('name','code','price',
      'category_id','description');
    foreach($fields as $field){
      $data[$field] = $this->input->post($field);
    }
    return $data;
  }

  public function admin(){
    $query = $this->Product_model->getAll();
    $data['query'] =  $query;
    $this->load->view('template/header');
    $this->load->view('product/admin_product',$data);
    $this->load->view('template/footer');
  }


}
